<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="PHP, form">
    <meta name="author" content="Luka Matkovic">

    <title>PHP Forms</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Serif:400,600" rel="stylesheet">

</head>
<body>
    <?php
    
        $name = $email = $phone = $dateofbirth = $gender = $comment = "";
        $nameErr = $emailErr = $phoneErr = $dateofbirthErr = $genderErr = "";

        if (isset($_GET['name'])) { $name = $_GET['name']; }
        if (isset($_GET['email'])) { $email = $_GET['email']; }
        if (isset($_GET['phone'])) { $phone = $_GET['phone']; }
        if (isset($_GET['dateofbirth'])) { $dateofbirth = $_GET['dateofbirth']; }
        if (isset($_GET['gender'])) { $gender = $_GET['gender']; }
        if (isset($_GET['comment'])) { $comment = $_GET['comment']; }

        if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
        if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
        if (isset($_GET['phoneErr'])) { $phoneErr = $_GET['phoneErr']; }
        if (isset($_GET['dateofbirthErr'])) { $dateofbirthErr = $_GET['dateofbirthErr']; }
        if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }

    ?>

    <section class="forma">
        <form action="register.php" method="post">  
            <fieldset>
                <legend>Form</legend>

                <label for="name">Name <span class="error"> * <?php echo $nameErr; ?><span></label><br>
                <input type="text" name="name" value="<?php echo $name; ?>"><br>

                <label for="email">E-mail <span class="error"> * <?php echo $emailErr; ?></span></label><br>
                <input type="email" name="email" placeholder="mail@mail.com" value="<?php echo $email; ?>"><br>

                <label for="phone">Telephone Number <span class="error"> * <?php echo $phoneErr; ?></span></label><br>
                <input type="number" name="phone" value="<?php echo $phone; ?>"><br>

                <label for="dateofbirth">Date of Birth <span class="error"> * <?php echo $dateofbirthErr; ?></span></label><br>
                <input type="date" name="dateofbirth" value="<?php echo $dateofbirth; ?>"><br>

                <label for="gender">Gender <span class="error"> * <?php echo $genderErr; ?></span><br></label>
                <input type="radio" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male">Male<br>
                <input type="radio" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female">Female<br>
                <input type="radio" name="gender" <?php if (isset($gender) && $gender=="other") echo "checked";?> value="other">Other<br>

                <label for="comment">Write Your Comments</label><br>
                <textarea name="comment" rows="10" cols="50"><?php echo $comment; ?></textarea><br>

            </fieldset>
            <input type="submit" value="Submit" class="save">
        </form>
    </section>
</body>
</html>