<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="keywords" content="PHP, form">
        <meta name="author" content="Luka Matkovic">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Serif:400,600" rel="stylesheet">
        <title>Formed fulfilled successfully!</title>
    </head>
    <body>
    <?php

        $name = $email = $phone = $dateofbirth = $gender = $comment = "";
        $nameErr = $emailErr = $phoneErr = $dateofbirthErr = $genderErr = "";

        if ($_SERVER ["REQUEST_METHOD"] == "POST") {

            if (empty($_POST["name"])) {
            $nameErr = "Name is required!";
            }
            else {
                $name=test_input($_POST["name"]);
                if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
                    $nameErr = "Only letters and white space allowed!"; 
                }
                if (strlen($name)<5) {
                    $nameErr = "Please enter full name and surname!";
                }
            }

            if (empty($_POST["email"])) {
            $emailErr = "E-mail is required!";
            }
            else {
                $email=test_input($_POST["email"]);
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $emailErr = "Invalid Email format"; 
                }
                if (strlen($email)< 12) {
                    $emailErr = "Email address has to contain at least 12 characters.";
                }
                if (checkEmailAddress($email)===false) {
                    $emailErr = "Wrong Email domain!";
                }
            }


            if (empty($_POST["phone"])) {
            $phoneErr = "Phone is required!";
            }
            else {
            $phone=test_input($_POST["phone"]);
            }   

            if (empty($_POST["dateofbirth"])) {
            $dateofbirthErr = "Date of birth is required!";
            }
            else {
                $dateofbirth=test_input($_POST["dateofbirth"]);
                $date=date_create($_POST['dateofbirth']);
                $date=date_format($date,"l, F j Y");
            }  
            $year=substr($_POST["dateofbirth"],0,4);
            (int)$year;
            if ($year<1900 or $year>2017) {
                $dateofbirthErr = "Invalid year";
            }

            if (empty($_POST["gender"])) {
            $genderErr = "Gender is required!";
            }
            else {
            $gender=test_input($_POST["gender"]);
            }

            if (empty($_POST["comment"])) {
            $comment = "";
            }
            else {
            $comment = test_input($_POST["comment"]);
            }

            if (!empty($nameErr) or !empty($emailErr) or !empty($dateofbirthErr) or !empty($phoneErr) or !empty($genderErr)) {
                $params = "name=" . urlencode($_POST["name"]);
                $params.= "&email=" . urlencode($_POST["email"]);
                $params.="&phone=" . urlencode($_POST["phone"]);
                $params.="&dateofbirth=" . urlencode($_POST["dateofbirth"]);
                $params.="&gender="  .urlencode($_POST["gender"]);
                $params.="&comment=" . urlencode($_POST["comment"]);

                $params.="&nameErr=" . urlencode($nameErr);
                $params.="&emailErr=" . urlencode($emailErr);
                $params.="&phoneErr=" . urlencode($phoneErr);
                $params.="&dateofbirthErr=" . urlencode($dateofbirthErr);
                $params.="&genderErr="  .urlencode($genderErr);

                header("Location: index.php?".$params);
            }
            else {
                echo "<div class=\"rez\">";
                echo "Welcome ".$_POST['name']."<br>";
                echo "Your email address is: ".$_POST['email']."<br>";
                echo "Your date of birth is: ".$date."<br>";
                echo "Your telephone is: ". $_POST['phone']."<br>";
                echo "Additional information: ".$_POST['comment']."<br>";
                echo "<a href=\"index.php\">Return to form</a>";
                echo "</div>";
    
            }
        }

        function test_input($data) {
            $data=trim($data);
            $data=stripslashes($data);
            $data=htmlspecialchars($data);
            return $data;
        }

        function checkEmailAddress ($email) {
            if (stristr($email,"@gmail.com",true)) {
                return true;
            }
            else {
                return false;
            }
        }
    ?>
    </body>
</html>