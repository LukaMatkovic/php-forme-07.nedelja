index.php i register.php dopuniti tako da:

1) Sadrzi polje za unos datuma rodenja, za ovo polje napraviti validaciju i poruku za gre�ku za slucaj da nije popunjeno.

2) Na register2.php stranici dodati ispis datuma i dodati da se ispis`e i dan u nedelji na engleskom jeziku.

3) Napraviti dodatnu proveru za du�inu imena i prezimena i email adrese. Minimalna du�ina imena i prezimena je 2 znaka, a email adrese 6 znakova.

4) Napraviti dodatnu proveru email adrese, pri cemu je kriterijum da email adresa pripada domenu \@gmail.com. U suprotnom ispisati odgovarajucu gre�ku. 
Za potrebe provere email adrese napraviti funkciju checkEmailAdress koja kao parametar prihvata email adresu, a kao rezultat vraca vrednost logickog tipa - true ili false.